#!/usr/bin/env python

import sys
import argparse

import boto.ec2

class AWSController:

  ### make sure your ~/.boto file is correct (aws_iam_id/aws_iam_key)
  defaults = {
    'region' : 'us-west-2',
    'security_group' : 'permit-ssh-any',
    'ami_id' : 'ami-6ac2a85a',
    'instance_type' : 't1.micro',
    'key_name' : 'awskey',
    'shutdown_behavior' : 'terminate',
    'aws_iam_id' : None,
    'aws_iam_key' : None,
  }

  ### TODO: better defaults/properties handling
  def __init__(self, region=defaults['region']):
    self.defaults = AWSController.defaults
    self.connection = self.connect(region)

  def connect(self, region=defaults['region']):
    c = None
    try:
      c = boto.ec2.connect_to_region(region)
      return c
    except Exception as e:
      print "Connection error: %s" % (e.message)
      sys.exit(1)

  def disconnect(self):
    return self.connection.close()

  ##########################################
  ### list
  ##########################################

  ### TODO: detailed/verbose listings
  def list_instances(self):
    fmtstr = "{id:12s}{state:15s}{instance_type:15s}{ip_address:17s}{launch_time:s}"
    ins = self.connection.get_only_instances()

    if len(ins) > 0:
      print fmtstr.format(**{
        'id':'id',
        'state' : 'state',
        'instance_type' : 'instance_type',
        'ip_address' : 'ip_address',
        'launch_time' : 'launch_time',
      })

    for i in ins:
      status = {}
      i.update()
      status['state'] = i.state ### weird compound value not in __dict__
      for k in i.__dict__.keys():
        status[k] = i.__dict__[k]
      print fmtstr.format(**status)


  ##########################################
  ### create
  ##########################################

  ### TODO: non-default instance profiles
  def create_instances(self, profile, num):
    if profile.lower() == 'default':
      for n in range(num):
        self.create_default_instance()
    else:
      pass

  def create_default_instance(self):
    return self.connection.run_instances(
      self.defaults['ami_id'],
      instance_type = self.defaults['instance_type'],
      security_groups = [ self.defaults['security_group'] ],
      key_name = self.defaults['key_name'],
      instance_initiated_shutdown_behavior = self.defaults['shutdown_behavior'],
    )

  ##########################################
  ### start/stop/terminate
  ##########################################

  def act_on_instances(self, action, instance_ids):
    hasall = lambda i : str(i).lower() == 'all'
    instances = []
    verb = str(action).lower()

    if len(filter(hasall, instance_ids)) > 0:
      instances = self.connection.get_only_instances()
    else:
      instances = self.connection.get_only_instances(instance_ids)

    ### let AWS barf on invalid state changes
    ### but catch Exception and print non-XML msg
    for i in instances:
      try:
        if verb ==   'start': i.start()
        elif verb == 'stop' : i.stop()
        elif verb == 'term' : i.terminate()
        else: pass
      except Exception as e:
        print "Error: %s" % (e.message)

### end AWSController

def parse_commandline():
  p = argparse.ArgumentParser(description='Manage AWS EC2 Instances')
  
  p.add_argument('--list', action='store_true',
                  help='List summary of all instances')

  ### verbose 

  p.add_argument('--create', metavar='profile',
                  help='Create --create_num instances with \'profile\'. '
                        'Currently \'default\' is only accepted value.')
  
  p.add_argument('--create_num', nargs='?', metavar='count', default=1,
                  type=int, help='Number of --create instances to create. '
                        'Defaults to 1'
                )
  
  p.add_argument('--start', nargs='+', metavar='id',
                  help='Start existing instances by id. Use \'ALL\' as id to '
                       'start all stopped instances')
  
  p.add_argument('--stop', nargs='+', metavar='id',
                  help='Stop existing instances by id. Use \'ALL\' as id to '
                       'stop all started instances')
  
  p.add_argument('--term', nargs='+', metavar='id',
                  help='Terminate existing instances by id. Use \'ALL\' as id to '
                       'terminate all non-terminated instances')

  ### TODO: specify region via command line

  args = p.parse_args()
  return args
  
if __name__ == "__main__":
  args = parse_commandline()

  ctl = AWSController()

  if args.list:
    ctl.list_instances()
  if args.create:
    ctl.create_instances(args.create, args.create_num)
  if args.start:
    ctl.act_on_instances('start', args.start)
  if args.stop:
    ctl.act_on_instances('stop', args.stop)
  if args.term:
    ctl.act_on_instances('term', args.term)

  ctl.disconnect()

