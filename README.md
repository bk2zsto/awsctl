
    usage: awsctl.py [-h] [--list] [--create profile] [--create_num [count]]
                     [--start id [id ...]] [--stop id [id ...]]
                     [--term id [id ...]]
    
    Manage AWS EC2 Instances
    
    optional arguments:
      -h, --help            show this help message and exit
      --list                List summary of all instances
      --create profile      Create --create_num instances with 'profile'.
                            Currently 'default' is only accepted value.
      --create_num [count]  Number of --create instances to create. Defaults to 1
      --start id [id ...]   Start existing instances by id. Use 'ALL' as id to
                            start all stopped instances
      --stop id [id ...]    Stop existing instances by id. Use 'ALL' as id to stop
                            all started instances
      --term id [id ...]    Terminate existing instances by id. Use 'ALL' as id to
                            terminate all non-terminated instances
